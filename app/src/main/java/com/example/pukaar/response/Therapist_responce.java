package com.example.pukaar.response;

import com.google.gson.annotations.SerializedName;

public class Therapist_responce {
    @SerializedName("id")
    public Integer id = 0;
    @SerializedName("about")
    public String about = "";
    @SerializedName("city")
    public String city = "";
    @SerializedName("service_therapist_provider")
    public String service_therapist_provider = "";
    @SerializedName("therapist_focus")
    public String therapist_focus = "";
    @SerializedName("type_of_doctor")
    public String type_of_doctor = "";
    @SerializedName("introduction")
    public String introduction = "";
    @SerializedName("education")
    public String education = "";

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @SerializedName("user")
    public User user;


    public class User{
        @SerializedName("id")
        public Integer id = 0;
        @SerializedName("first_name")
        public String first_name = "";
        @SerializedName("last_name")
        public String last_name = "";
        @SerializedName("mobile_number")
        public String mobile_number = "";

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getMobile_number() {
            return mobile_number;
        }

        public void setMobile_number(String mobile_number) {
            this.mobile_number = mobile_number;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        @SerializedName("email")
        public String email = "";
        @SerializedName("created_at")
        public String created_at = "";
        @SerializedName("updated_at")
        public String updated_at = "";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getService_therapist_provider() {
        return service_therapist_provider;
    }

    public void setService_therapist_provider(String service_therapist_provider) {
        this.service_therapist_provider = service_therapist_provider;
    }

    public String getTherapist_focus() {
        return therapist_focus;
    }

    public void setTherapist_focus(String therapist_focus) {
        this.therapist_focus = therapist_focus;
    }

    public String getType_of_doctor() {
        return type_of_doctor;
    }

    public void setType_of_doctor(String type_of_doctor) {
        this.type_of_doctor = type_of_doctor;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }


}
