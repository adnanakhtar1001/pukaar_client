package com.example.pukaar.response;

import com.google.gson.annotations.SerializedName;

public class BankDetailResponse {

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountTitle() {
        return accountTitle;
    }

    public void setAccountTitle(String accountTitle) {
        this.accountTitle = accountTitle;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    @SerializedName("id")
    public Integer id = 0;
    @SerializedName("bank_name")
    public String bankName = "";
    @SerializedName("branch_name")
    public String branchName = "";
    @SerializedName("account_number")
    public String accountNumber = "";
    @SerializedName("account_title")
    public String accountTitle = "";
    @SerializedName("iban")
    public String iban = "";

}
