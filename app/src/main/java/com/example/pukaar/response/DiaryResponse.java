package com.example.pukaar.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DiaryResponse {


    @SerializedName("data")
    public FirstDatum Fdata = null;

    public class FirstDatum {
        @SerializedName("current_page")
        public int currentPage = 0;

        @SerializedName("data")
        public ArrayList<Datum> data = null;

        @SerializedName("first_page_url")
        public String firstPageUrl = "";

        @SerializedName("from")
        public int from = 1;

        @SerializedName("last_page")
        public int lastPage = 1;

        @SerializedName("last_page_url")
        public String lastPageUrl = "";

        @SerializedName("next_page_url")
        public String nextPageUrl = "";

        @SerializedName("path")
        public String path = "";

        @SerializedName("per_page")
        public int perPage = 0;

        @SerializedName("prev_page_url")
        public String prevPageUrl = "";

        @SerializedName("to")
        public int to = 0;

        @SerializedName("total")
        public int total = 0;



    public class Datum {
        @SerializedName("id")
        public int id;

        @SerializedName("mood")
        public String mood;

        @SerializedName("anxiety")
        public String anxiety;

        @SerializedName("energy")
        public String energy;

        @SerializedName("self_confidence")
        public String selfConfidence;

        @SerializedName("feeling")
        public String feeling;

        @SerializedName("created_at")
        public String createdAt;

        @SerializedName("updated_at")
        public String updatedAt;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMood() {
            return mood;
        }

        public void setMood(String mood) {
            this.mood = mood;
        }

        public String getAnxiety() {
            return anxiety;
        }

        public void setAnxiety(String anxiety) {
            this.anxiety = anxiety;
        }

        public String getEnergy() {
            return energy;
        }

        public void setEnergy(String energy) {
            this.energy = energy;
        }

        public String getSelfConfidence() {
            return selfConfidence;
        }

        public void setSelfConfidence(String selfConfidence) {
            this.selfConfidence = selfConfidence;
        }

        public String getFeeling() {
            return feeling;
        }

        public void setFeeling(String feeling) {
            this.feeling = feeling;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getFirstPageUrl() {
        return firstPageUrl;
    }

    public void setFirstPageUrl(String firstPageUrl) {
        this.firstPageUrl = firstPageUrl;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getLastPage() {
        return lastPage;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    public String getLastPageUrl() {
        return lastPageUrl;
    }

    public void setLastPageUrl(String lastPageUrl) {
        this.lastPageUrl = lastPageUrl;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getPrevPageUrl() {
        return prevPageUrl;
    }

    public void setPrevPageUrl(String prevPageUrl) {
        this.prevPageUrl = prevPageUrl;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
    }

}
