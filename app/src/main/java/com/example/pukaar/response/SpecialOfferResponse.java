package com.example.pukaar.response;

import com.google.gson.annotations.SerializedName;

public class SpecialOfferResponse {

    @SerializedName("id")
    public Integer id = 0;
    @SerializedName("number_of_sessions")
    public Integer session = 0;
    @SerializedName("price")
    public String price = "";
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSession() {
        return session;
    }

    public void setSession(Integer session) {
        this.session = session;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

}
