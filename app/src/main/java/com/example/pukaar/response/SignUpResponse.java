package com.example.pukaar.response;

import com.google.gson.annotations.SerializedName;

public class SignUpResponse {


    @SerializedName("data")
    public Datum data = null;

    public class Datum {

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }


        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        @SerializedName("first_name")
        public String first_name;
        @SerializedName("last_name")
        public String last_name;
        @SerializedName("token")
        public String token;
        @SerializedName("user_id")
        public Integer id;
        @SerializedName("email")
        public String email;
        @SerializedName("role")
        public String role;
    }
    public Datum getData() {
        return data;
    }

    public void setData(Datum data) {
        this.data = data;
    }
}
