package com.example.pukaar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pukaar.R;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Edit_user_profile extends AppCompatActivity {

    @BindView(R.id.back_arrow_013)
    ImageView back_arrow_013;
    @BindView(R.id.bottom_navigation14)
    BottomNavigationView bottomNavigationView14;
    @BindView(R.id.save_profile)
    Button save_profile;
    @BindView(R.id.user_name_edit)
    EditText pateint_name;
    @BindView(R.id.user_email_edit)
    EditText patenient_email;
    private CommonFunction commonFunction;
    private APIInterface apiInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_profile);
        ButterKnife.bind(this);
        ImageView bell =findViewById(R.id.eup_setting);
        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Edit_user_profile.this , Setting.class);
                startActivity(intent);
            }
        });
        apiInterface = APIClient.getClient().create(APIInterface .class);
        String first_name =commonFunction.getname(this);
        String email = commonFunction.getEmail(this);
        pateint_name.setText(first_name);
        patenient_email.setText(email);

        back_arrow_013.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Edit_user_profile.this, Profile.class));
                overridePendingTransition(0,0);
            }
        });
        save_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = pateint_name.getText().toString();
                String[] parts = name.split(" ");
                String first_name1 = parts[0];
                String last_name1 = parts[1];
                Call<String> update_data = apiInterface.updateProfile(CommonFunction.getToken(getApplicationContext()) , first_name1 ,  last_name1 , patenient_email.getText().toString());
                update_data.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.body() != null){
                            CommonFunction.setName(pateint_name.getText().toString() , getApplicationContext());
                            CommonFunction.setEmail(patenient_email.getText().toString() ,getApplicationContext());
                            startActivity(new Intent(Edit_user_profile.this, Profile.class));
                            overridePendingTransition(0,0);
                            Toast.makeText(getApplication(), "Uptaded Sucessfully", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                    }
                });


            }
        });

        bottomNavigationView14.getMenu().findItem(R.id.nav_home).setChecked(true);


        bottomNavigationView14.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        startActivity(new Intent(getApplication(), Dashboard.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_room:
                        startActivity(new Intent(getApplication(), Session_room.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_profile:
                        startActivity(new Intent(getApplication(), Profile.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.bottom_settting:
                        startActivity(new Intent(getApplication(), Setting.class));
                        overridePendingTransition(0,0);
                        break;
                }
                return false;
            }
        });
    }
}