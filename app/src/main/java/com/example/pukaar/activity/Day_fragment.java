package com.example.pukaar.activity;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pukaar.R;
import com.example.pukaar.adapter.DiaryViewAdapter;
import com.example.pukaar.response.DiaryResponse;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Day_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Day_fragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private DiaryResponse responseData = null;
    private RecyclerView recyclerView;
    private DiaryViewAdapter adapter;

    public Day_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Day_fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Day_fragment newInstance(String param1, String param2) {
        Day_fragment fragment = new Day_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_day_fragment, container, false);
        initView(view);
        setRecyclerView();
        return view;
    }

    private void setRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new DiaryViewAdapter(getActivity(), responseData.Fdata.data);
        recyclerView.setAdapter(adapter);
    }

    private void initView(View view) {
        recyclerView =(RecyclerView) view.findViewById(R.id.diaryRecyclerView);

    }

    public void setResponse(DiaryResponse respones) {
        this.responseData = respones;
    }
}