package com.example.pukaar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.pukaar.R;
import com.example.pukaar.adapter.Forum_Adapter;
import com.example.pukaar.adapter.Notification_Adapter;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;
import com.example.pukaar.response.ForumResponse;
import com.example.pukaar.response.MessageResponse;
import com.example.pukaar.response.NotificationResponse;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

import java.net.URISyntaxException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notifications extends AppCompatActivity {
    @BindView(R.id.back_arrow_13)
    ImageView back_arrow_13;
    @BindView(R.id.bottom_navigation1)
    BottomNavigationView bottomNavigationView;
    private APIInterface apiInterface;
    public Notification_Adapter notification_adapter;
    public ArrayList<NotificationResponse> notificationResponse;
    RecyclerView recyclerView;


    private Socket mSocket;

    {
        try {
            mSocket = IO.socket("http://pukar.qareeb.com/api");
        } catch (URISyntaxException e) {
            Log.d("myTag", e.getMessage());
            //e.getMessage();
        }
    }

    private void connectToSignallingServer() {
        try {
            String URL = "http://185.206.135.170:8005";
            mSocket = IO.socket(URL);

            mSocket.on("connect", args -> {
                Log.d("hello", "connectToSignallingServer: connect");
                mSocket.emit("user_connected", CommonFunction.getUserId(getApplicationContext()));
            });
            mSocket.on("notification-channel:App\\Events\\NotificationEvent",onNewMessage);
            mSocket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                    Gson gson = new Gson();
                    NotificationResponse response = gson.fromJson(args[0].toString(), NotificationResponse.class);
                    notificationResponse.add(response);
                    notification_adapter.notifyDataSetChanged();
                    recyclerView.scrollToPosition(recyclerView.getAdapter().getItemCount() - 1);
                    }catch (Exception ex){}               }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);


         ProgressDialog progressDialog = new ProgressDialog(Notifications.this);
        progressDialog.setMessage("please wait Data is Fetching...");
        progressDialog.setTitle("Data Fetching");
        progressDialog.setCancelable(false);
        progressDialog.show();

        recyclerView = findViewById(R.id.notification_recycler);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ArrayList<NotificationResponse>> call = apiInterface.getNotification(CommonFunction.getToken(getApplicationContext()));
        call.enqueue(new Callback<ArrayList<NotificationResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<NotificationResponse>> call, Response<ArrayList<NotificationResponse>> response) {
                if (response.body() != null){

                notificationResponse = response.body();
                notification_adapter = new Notification_Adapter(getApplicationContext(),notificationResponse);
                //recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                LinearLayoutManager llm = new LinearLayoutManager(Notifications.this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(llm);
                recyclerView.setAdapter(notification_adapter);
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<NotificationResponse>> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
        back_arrow_13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Notifications.this ,Dashboard.class));
                overridePendingTransition(0,0);
            }
        });
        bottomNavigationView.getMenu().findItem(R.id.nav_home).setChecked(true);


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        startActivity(new Intent(getApplication(), Dashboard.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_room:
                        startActivity(new Intent(getApplication(), Session_room.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_profile:
                        startActivity(new Intent(getApplication(), Profile.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.bottom_settting:
                        startActivity(new Intent(getApplication(), Setting.class));
                        overridePendingTransition(0,0);
                        break;


                }
                return false;
            }
        });

    }
}