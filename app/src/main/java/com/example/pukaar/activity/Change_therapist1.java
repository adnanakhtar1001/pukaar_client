package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Change_therapist1 extends AppCompatActivity {


    @BindView(R.id.back_arrow_17)
    ImageView back_arrow_17;
    @BindView(R.id.ch1_setting)
    ImageView ch1_setting;

    @BindView(R.id.change_therapist_ok_button)
    Button change_therapist_ok_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_therapist1);
        ButterKnife.bind(this);
        back_arrow_17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Change_therapist1.this, Change_therapist.class));
                overridePendingTransition(0,0);
            }
        });
        ch1_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Change_therapist1.this, Setting.class));
                overridePendingTransition(0,0);
            }
        });

        change_therapist_ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Change_therapist1.this, Dashboard.class));
                overridePendingTransition(0,0);
            }
        });


    }
}