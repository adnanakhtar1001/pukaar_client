package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Notification_setting extends AppCompatActivity {

    @BindView(R.id.back_arrow_25)
    ImageView back_arrow_25;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_setting);
        ButterKnife.bind(this);
        ImageView bell =findViewById(R.id.ns_bell);
        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_setting.this , Notifications.class);
                startActivity(intent);
            }
        });
        back_arrow_25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Notification_setting.this,Setting.class));
                overridePendingTransition(0,0);
            }
        });
    }
}