package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.pukaar.R;
import com.example.pukaar.adapter.Forum_Adapter;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;
import com.example.pukaar.response.ForumResponse;
import com.example.pukaar.response.SpecialOfferResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Pukaar_Forum extends AppCompatActivity {
    private APIInterface apiInterface;
    public Forum_Adapter forum_adapter;
    public ForumResponse forumResponse;
    RecyclerView recyclerView;
    Button create_post;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pukaar_forum);
        ImageView bell =findViewById(R.id.forum_bell);
        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Pukaar_Forum.this , Notifications.class);
                startActivity(intent);
            }
        });
        ImageView backarrow= findViewById(R.id.back_arrow_forum);
        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recyclerView = findViewById(R.id.forum_recycler);
        create_post = findViewById(R.id.create_post_button);
        ProgressDialog progressDialog = new ProgressDialog(Pukaar_Forum.this);
        progressDialog.setMessage("please wait Pukaar Forum is Fetching...");
        progressDialog.setTitle("Data Fetching");
        progressDialog.setCancelable(false);
        progressDialog.show();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ForumResponse> call = apiInterface.getForumList(CommonFunction.getToken(getApplicationContext()));
        call.enqueue(new Callback<ForumResponse>() {
            @Override
            public void onResponse(Call<ForumResponse> call, Response<ForumResponse> response) {
                if (response.body() != null){
                 progressDialog.dismiss();
                    forum_adapter = new Forum_Adapter(getApplicationContext(), response.body().Fdata);
                    //recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                LinearLayoutManager llm = new LinearLayoutManager(Pukaar_Forum.this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(llm);
                    recyclerView.setAdapter(forum_adapter);}

            }

            @Override
            public void onFailure(Call<ForumResponse> call, Throwable t) {
                progressDialog.dismiss();

            }
        });
 create_post.setOnClickListener(new View.OnClickListener() {
     @Override
     public void onClick(View v) {
         Intent  intent= new Intent(Pukaar_Forum.this , My_Post.class);
         startActivity(intent);
     }
 });
    }
}