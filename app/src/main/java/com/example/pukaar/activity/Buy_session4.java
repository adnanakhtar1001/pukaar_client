package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Buy_session4 extends AppCompatActivity {
    @BindView(R.id.ok_button)
    Button ok_button;
    @BindView(R.id.back_arrow_6)
    ImageView back_arrow_6;
    @BindView(R.id.buy_donate_session4)
    TextView buy_donate_session4;
    String buy_donate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_session4);
         ButterKnife.bind(this);
        ImageView bell =findViewById(R.id.bell_bs4);
        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Buy_session4.this , Notifications.class);
                startActivity(intent);
            }
        });
         Intent intent = getIntent();
         buy_donate = intent.getStringExtra("buy");
         buy_donate_session4.setText(buy_donate + " Session");

        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               startActivity(new Intent(Buy_session4.this ,Dashboard.class));
                overridePendingTransition(0,0);
                finish();
            }
        });
        back_arrow_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Buy_session4.this ,Buy_session7.class));
                overridePendingTransition(0,0);
            }
        });
    }
}