package com.example.pukaar.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.pukaar.R;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Right_therapist extends AppCompatActivity {
    @BindView(R.id.reight_therapist_button)
    Button right_therapist_button;

    @BindView(R.id.orientation)
    AutoCompleteTextView orientation;

    @BindView(R.id.religious)
    AutoCompleteTextView religious;

    @BindView(R.id.identify)
    AutoCompleteTextView identify;

    @BindView(R.id.medicine)
    AutoCompleteTextView medicine;

    @BindView(R.id.suffering)
    EditText suffering;

    @BindView(R.id.good)
    ImageView good;

    @BindView(R.id.fair)
    ImageView fair;

    @BindView(R.id.bad)
    ImageView bad;

    String selectedText = "Good";
    private APIInterface apiInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_right_therapist);
        ButterKnife.bind(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        setImageColor();
        orientation.setRawInputType(InputType.TYPE_NULL);
        religious.setRawInputType(InputType.TYPE_NULL);
        identify.setRawInputType(InputType.TYPE_NULL);
        medicine.setRawInputType(InputType.TYPE_NULL);


        ArrayAdapter<String> myAdapter = new ArrayAdapter(Right_therapist.this,R.layout.spinner_dropdown_itemlist, getResources().getStringArray(R.array.orientation));
        orientation.setAdapter(myAdapter);
        //
        ArrayAdapter<String> relegious = new ArrayAdapter(Right_therapist.this,R.layout.spinner_dropdown_itemlist, getResources().getStringArray(R.array.relegoius));
        religious.setAdapter(relegious);

        ArrayAdapter<String> identify1 = new ArrayAdapter(Right_therapist.this,R.layout.spinner_dropdown_itemlist, getResources().getStringArray(R.array.relegion));
        identify.setAdapter(identify1);

        ArrayAdapter<String> medicine1 = new ArrayAdapter(Right_therapist.this,R.layout.spinner_dropdown_itemlist, getResources().getStringArray(R.array.medicine));
        medicine.setAdapter(medicine1);
    }
    @OnClick(R.id.reight_therapist_button)
    void buttonClick(View view) {
        /*startActivity(new Intent(Right_therapist.this ,Congratulation.class));
        overridePendingTransition(0,0);*/
        if(IsValid())
        {
            Call<String> call = apiInterface.clientCreateProfile(CommonFunction.getToken(getApplicationContext()),orientation.getText().toString(),religious.getText().toString(),identify.getText().toString(),medicine.getText().toString(),selectedText,suffering.getText().toString());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if(response.body() != null)
                    {
                        startActivity(new Intent(Right_therapist.this ,Congratulation.class));
                        overridePendingTransition(0,0);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                }
            });
        }
    }
    private boolean IsValid()
    {
        if(orientation.getText().toString().trim().length() == 0) {
            Toast.makeText(getApplicationContext(),"Please Enter Orientation Value",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(religious.getText().toString().trim().length() == 0) {
            Toast.makeText(getApplicationContext(),"Please Enter Religious Value",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(identify.getText().toString().trim().length() == 0) {
            Toast.makeText(getApplicationContext(),"Please Enter Identify Value",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(medicine.getText().toString().trim().length() == 0) {
            Toast.makeText(getApplicationContext(),"Please Enter Medicine Value",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(suffering.getText().toString().trim().length() == 0) {
            Toast.makeText(getApplicationContext(),"Please Enter Suffering Value",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
    @OnClick(R.id.bad)
    void badClick(View view) {
        selectedText = "Bad";
        bad.setColorFilter(getResources().getColor(R.color.text_blue));
        fair.setColorFilter(getResources().getColor(R.color.unselectedColor));
        good.setColorFilter(getResources().getColor(R.color.unselectedColor));
    }
    @OnClick(R.id.good)
    void goodClick(View view) {
        selectedText = "Good";
        bad.setColorFilter(getResources().getColor(R.color.unselectedColor));
        fair.setColorFilter(getResources().getColor(R.color.unselectedColor));
        good.setColorFilter(getResources().getColor(R.color.text_blue));
    }
    @OnClick(R.id.fair)
    void fairClick(View view) {
        selectedText = "Fair";
        bad.setColorFilter(getResources().getColor(R.color.unselectedColor));
        fair.setColorFilter(getResources().getColor(R.color.text_blue));
        good.setColorFilter(getResources().getColor(R.color.unselectedColor));
    }
    private void setImageColor() {
        bad.setColorFilter(getResources().getColor(R.color.text_blue));
        fair.setColorFilter(getResources().getColor(R.color.unselectedColor));
        good.setColorFilter(getResources().getColor(R.color.unselectedColor));
    }

}