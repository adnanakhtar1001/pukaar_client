package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pukaar.R;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Reuest_1on1_session extends AppCompatActivity {
    @BindView(R.id.back_arrow_12)
    ImageView back_arrow_12;
    @BindView(R.id.one_request_button)
    Button one_request_button;
    @BindView(R.id.calendarView)
    CalendarView calendarView;
    String  curDate;
    String  Year;
    String  Month;
    String formattedDate;
    private APIInterface apiInterface;
    Dialog dialog;
    ProgressDialog progressDialog;
    TextView error;
    Button yes;
    Button no;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reuest1on1_session);
        ButterKnife.bind(this);

        ImageView bell =findViewById(R.id.req_bell);
        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Reuest_1on1_session.this , Notifications.class);
                startActivity(intent);
            }
        });
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        formattedDate = df.format(c);

        String[] parts = formattedDate.split("-");
        curDate = parts[0];
        Month = parts[1];
        Year = parts[2];

        calendarView.setMinDate(System.currentTimeMillis() - 1000);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                  curDate = String.valueOf(dayOfMonth);
                 Year = String.valueOf(year);
                  Month = String.valueOf(month+1);

               /* Log.e("date",Year+"/"+Month+"/"+curDate);*/
            }
        });


        back_arrow_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Reuest_1on1_session.this ,Dashboard.class));
                overridePendingTransition(0,0);
            }
        });
        one_request_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(Reuest_1on1_session.this);
                progressDialog.setMessage("please wait for Request...");
                progressDialog.setTitle("Request Pending");
                progressDialog.setCancelable(false);

                progressDialog.show();
                Call<Object> call = apiInterface.OneonOneSession(CommonFunction.getToken(getApplicationContext()), curDate+"-"+Month+"-"+Year);
                call.enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        if (response.body() != null) {
                            progressDialog.dismiss();
   if (response.body().toString().toLowerCase().equalsIgnoreCase("{response=Request already have been sent}")) {
                                dialog = new Dialog(Reuest_1on1_session.this);
                                dialog.setContentView(R.layout.dialuge_box1);
                                dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialgue_box);
                                TextView    error2 = dialog.findViewById(R.id.error_text);
                                Button   yes2 = dialog.findViewById(R.id.yes_button1);

                                Button   no2 = dialog.findViewById(R.id.no_button1);
                                error2.setText("Request Already Have been Sent.");
                                dialog.show();

                                yes2.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Reuest_1on1_session.this, Buy_session1.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.putExtra("buy", "Buy");
                                        startActivity(intent);
                                    }
                                });

                                no2.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Reuest_1on1_session.this, Dashboard.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });

                            }
   else     if (response.body().toString().toLowerCase().equalsIgnoreCase("{response=Therapist Not Assigned}")) {
                                dialog = new Dialog(Reuest_1on1_session.this);
                                dialog.setContentView(R.layout.dialuge_box1);
                                dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialgue_box);
                             TextView   error = dialog.findViewById(R.id.error_text);
                             Button   yes = dialog.findViewById(R.id.yes_button1);
                             Button   no = dialog.findViewById(R.id.no_button1);
                                error.setText("Therapist is not Assigned Yet");
                                dialog.show();

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Reuest_1on1_session.this, Buy_session1.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.putExtra("buy", "Buy");
                                        startActivity(intent);
                                    }
                                });

                                no.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Reuest_1on1_session.this, Dashboard.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                            }
                            else   if (response.body().toString().toLowerCase().equalsIgnoreCase("{response=Your payment for one/one session is not submit yet}")) {
                                dialog = new Dialog(Reuest_1on1_session.this);
                                dialog.setContentView(R.layout.dialuge_box1);
                                dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialgue_box);
                           TextView     error1 = dialog.findViewById(R.id.error_text);
                            Button    yes1 = dialog.findViewById(R.id.yes_button1);

                            Button   no1 = dialog.findViewById(R.id.no_button1);
                                error1.setText("Your payment for one/one session is not submit yet");
                                dialog.show();

                                yes1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Reuest_1on1_session.this, Buy_session1.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.putExtra("buy", "Buy");
                                        startActivity(intent);
                                    }
                                });

                                no1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Reuest_1on1_session.this, Dashboard.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                                ;
                            }

                            else {
                                progressDialog.dismiss();
                             Intent intent = new Intent(Reuest_1on1_session.this , Requuest_sent.class);
                             intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                             startActivity(intent);
                            }
                        }
                        else {
                            dialog = new Dialog(Reuest_1on1_session.this);
                            dialog.setContentView(R.layout.dialuge_box1);
                            dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialgue_box);
                            TextView    error2 = dialog.findViewById(R.id.error_text);
                            Button   yes2 = dialog.findViewById(R.id.yes_button1);

                            Button   no2 = dialog.findViewById(R.id.no_button1);
                            error2.setText("Internal Server Error");
                            dialog.show();

                            yes2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Reuest_1on1_session.this, Buy_session1.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.putExtra("buy", "Buy");
                                    startActivity(intent);
                                }
                            });

                            no2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Reuest_1on1_session.this, Dashboard.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            });
                            ;
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        progressDialog.dismiss();

                    }
                });
               /* Toast.makeText(getApplicationContext(), curDate+"-"+Month+"-" +Year, Toast.LENGTH_SHORT).show();
               *//* startActivity(new Intent(Reuest_1on1_session.this ,Requuest_sent.class));
                overridePendingTransition(0,0);*/
            }
        });
    }
}

    /*Your payment for one/one session is not submit yet*/