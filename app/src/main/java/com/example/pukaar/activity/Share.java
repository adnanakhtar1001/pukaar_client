package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ShareActionProvider;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Share extends AppCompatActivity {
    @BindView(R.id.back_arrow_26)
    ImageView back_arrow_26;
    Button share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        ButterKnife.bind(this);
       share = findViewById(R.id.share_button);
        ImageView bell =findViewById(R.id.share_bell);
        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Share.this , Notifications.class);
                startActivity(intent);
            }
        });
        back_arrow_26.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Share.this,Setting.class));
                overridePendingTransition(0,0);
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "Your Subject");
                String sAux = "\nLet me recommend you this application\n\n";
                sAux = sAux + "http://pukaarcommunity.com/ \n\n"; // here define package name of you app
                i.putExtra(Intent.EXTRA_TEXT, sAux);
                startActivity(Intent.createChooser(i, "choose one"));
            }
        });
    }
}