package com.example.pukaar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.pukaar.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Session_room extends AppCompatActivity {
    @BindView(R.id.back_arrow_8)
    ImageView back_arrow_8;
     @BindView(R.id.ssr_setting)
    ImageView ssr_setting;

    @BindView(R.id.buy_more_session)
    Button buy_more_session;
    @BindView(R.id.bottom_navigation2)
    BottomNavigationView bottomNavigationView2;
    @BindView(R.id.session_room_button_ss)
    Button session_room_button_ss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_room);
        ButterKnife.bind(this);
        back_arrow_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Session_room.this ,Dashboard.class));
                overridePendingTransition(0,0);
            }
        });
        ssr_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Session_room.this ,Setting.class));
                overridePendingTransition(0,0);
            }
        });

        buy_more_session.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Session_room.this , Dashboard.class));
                overridePendingTransition(0,0);
            }
        });

        session_room_button_ss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Session_room.this , Session_summary.class));
                overridePendingTransition(0,0);
            }
        });
        bottomNavigationView2.getMenu().findItem(R.id.nav_room).setChecked(true);


        bottomNavigationView2.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        startActivity(new Intent(getApplication(), Dashboard.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_room:
                        startActivity(new Intent(getApplication(), Session_room.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_profile:
                        startActivity(new Intent(getApplication(), Profile.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.bottom_settting:
                        startActivity(new Intent(getApplication(), Setting.class));
                        overridePendingTransition(0,0);
                        break;


                }
                return false;
            }
        });


    }
}