package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Buy_session2 extends AppCompatActivity {
    @BindView(R.id.add_receipt)
    Button add_receipt;
    @BindView(R.id.back_arrow_2)
    ImageView back_arrow_2;
    @BindView(R.id.number_of_sessions)
    TextView number_of_sessions ;
    @BindView(R.id.total_amount_1)
    TextView total_amount;
    @BindView(R.id.bank_name1)
    TextView bank_name1;
    @BindView(R.id.branch_name1)
    TextView branch_name1;
    @BindView(R.id.account_number1)
    TextView account_number1;
    @BindView(R.id.account_title1)
    TextView account_title1;
    @BindView(R.id.iban1)
    TextView iban1;
    String buy_donate ="";
    @BindView(R.id.buy_donate_session)
    TextView buy_donate_session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_session2);

        ButterKnife.bind(this);
        ImageView bell =findViewById(R.id.bell_bs2);
        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Buy_session2.this , Notifications.class);
                startActivity(intent);
            }
        });
// Getting value from Buy Now Button
        Intent intent = getIntent();
        String total_sessions = intent.getStringExtra("quantity");
        String total_amount1 = intent.getStringExtra("total Price");
        total_amount.setText(total_amount1);
        number_of_sessions.setText(total_sessions);
// Getting Buy/Donate Details
        buy_donate = intent.getStringExtra("buy");
        buy_donate_session.setText(buy_donate + " Sessions");

//Getting Bank Details
        Intent intent4 = getIntent();
        String bank_name = intent4.getStringExtra("bank name");
        bank_name1.setText(bank_name);
        String branch_name = intent4.getStringExtra("branch name");
        branch_name1.setText(branch_name);
        String account_number = intent4.getStringExtra("account number");
        account_number1.setText(account_number);
        String account_title = intent4.getStringExtra("account title");
        account_title1.setText(account_title);
        String iban = intent4.getStringExtra("iban");
        iban1.setText(iban);



        add_receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Buy_session2.this ,Buy_session3.class);
                intent.putExtra("quantity",number_of_sessions.getText().toString());
                intent.putExtra("price",total_amount.getText().toString());
                intent.putExtra("buy" , buy_donate);
                startActivity(intent);
                overridePendingTransition(0,0);
            }
        });
        back_arrow_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}