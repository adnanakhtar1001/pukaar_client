package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Setting extends AppCompatActivity {

@BindView(R.id.app_passcode_button)
   Button app_passcode_button;
    @BindView(R.id.term_of_use_button)
    Button term_of_use_button;
    @BindView(R.id.privacy_policy_button)
    Button privacy_policy_button;
   @BindView(R.id.notification_button)
    Button notification_button;
   @BindView(R.id.share_button)
    Button share_button;
  @BindView(R.id.subscribe_button)
    Button subscribe_button;
 @BindView(R.id.back_arrow_21)
 ImageView back_arrow_21;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        ButterKnife.bind(this);
        ImageView bell =findViewById(R.id.seeting_bell);
        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this , Notifications.class);
                startActivity(intent);
            }
        });
app_passcode_button.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(Setting.this, Pass_code.class));
        overridePendingTransition(0,0);
    }
});
        term_of_use_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Setting.this, Terms_Conditions.class));
                overridePendingTransition(0,0);

            }
        });
        privacy_policy_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Setting.this, Privacy_Policy.class));
                overridePendingTransition(0,0);
            }
        });
        notification_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Setting.this, Notification_setting.class));
                overridePendingTransition(0,0);
            }
        });
        share_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Setting.this, Share.class));
                overridePendingTransition(0,0);
            }
        });
        subscribe_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Setting.this, Subscribe.class));
                overridePendingTransition(0,0);
            }
        });
        back_arrow_21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Setting.this, Dashboard.class));
                overridePendingTransition(0,0);
            }
        });

    }
}