package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Donated_session extends AppCompatActivity {
    @BindView(R.id.donated_session_button)
    Button donated_session_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donated_session);
        ButterKnife.bind(this);
        donated_session_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Donated_session.this ,Dashboard.class));
                overridePendingTransition(0,0);
            }
        });
    }
}