package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pukaar.R;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.response.LoginResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {
    @BindView(R.id.login_screen_button)
    Button login_screen_button;
    @BindView(R.id.login_signup_button)
    TextView login_signup_button;
    @BindView(R.id.orientation)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    private APIInterface apiInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        login_screen_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email.getText().toString().trim().length()>5 && password.getText().toString().trim().length()>5) {
                    Call<LoginResponse> call = apiInterface.loginCall(email.getText().toString(),password.getText().toString());
                    call.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                           /* startActivity(new Intent(Login.this ,Dashboard.class));
                            overridePendingTransition(0,0);*/
                            Toast.makeText(getApplicationContext(),call.toString(),Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            Toast.makeText(getApplicationContext(),"Invalid Email and password",Toast.LENGTH_SHORT).show();
                        }
                    });
                }


            }
        });

        login_signup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this , Register.class));
                overridePendingTransition(0,0);
            }
        });
    }
}