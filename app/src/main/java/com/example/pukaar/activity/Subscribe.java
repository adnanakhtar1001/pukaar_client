package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Subscribe extends AppCompatActivity {
    @BindView(R.id.back_arrow_27)
    ImageView back_arrow_27;
    Button button;
    RadioButton radioButton1;
    RadioButton radioButton2;
    RadioButton radioButton3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);
        ButterKnife.bind(this);
       radioButton1 = findViewById(R.id.radioButton4);
       radioButton2 = findViewById(R.id.radioButton5);
       radioButton3 = findViewById(R.id.radioButton6);
        ImageView bell =findViewById(R.id.subs_bell);
        button = findViewById(R.id.subscribe_button);
        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Subscribe.this , Notifications.class);
                startActivity(intent);
            }
        });
        back_arrow_27.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Subscribe.this,Setting.class));
                overridePendingTransition(0,0);
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioButton1.isChecked()){
                    Uri uri = Uri.parse("http://pukaarcommunity.com/"); // missing 'http://' will cause crashed
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                else if (radioButton2.isChecked()){
                    Uri uri = Uri.parse("https://twitter.com/pukaarcommunity?t=TaHOV1dYUBT_Nxh-UZpQIA&s=09"); // missing 'http://' will cause crashed
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                else {
                    Uri uri = Uri.parse("https://www.facebook.com/pukaarcommunity/"); // missing 'http://' will cause crashed
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);

                }

            }
        });
    }
}