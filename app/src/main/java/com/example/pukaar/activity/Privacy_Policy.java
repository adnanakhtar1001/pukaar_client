package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Privacy_Policy extends AppCompatActivity {
    @BindView(R.id.back_arrow_24)
    ImageView back_arrow_24;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        ButterKnife.bind(this);
        ImageView bell =findViewById(R.id.pp_bell);
        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Privacy_Policy.this , Notifications.class);
                startActivity(intent);
            }
        });
        back_arrow_24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });
    }
}