package com.example.pukaar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pukaar.R;
import com.example.pukaar.common.CommonFunction;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Dashboard extends AppCompatActivity  {
    @BindView(R.id.cardView)
    CardView cardView;
    @BindView(R.id.cardView2)
    CardView cardView2;
    @BindView(R.id.cardView3)
    CardView cardView3;
    @BindView(R.id.cardView4)
    CardView cardView4;
    @BindView(R.id.cardView5)
    CardView cardView5;
    @BindView(R.id.cardView6)
    CardView cardView6;
    @BindView(R.id.hamburger)
    ImageView hamburger;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.set_your_mode_button)
    Button set_your_mode_button;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.User_FirstName)
    TextView User_FirstName;
    String s= "Buy";
    String s1 = "Donate";
    private CommonFunction commonFunction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_activity);
        ButterKnife.bind(this);
// Setting user name
String first_name =commonFunction.getname(this);
User_FirstName.setText(first_name);


        ImageView bell =findViewById(R.id.bellIcaon_Dashboard);
        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this , Notifications.class);
                startActivity(intent);
            }
        });
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, Session_room.class));
                overridePendingTransition(0,0);
            }
        });
        cardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, Buy_session1.class);
                intent.putExtra("buy" , s);
                startActivity(intent);

                overridePendingTransition(0,0);
            }
        });
        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, Buy_session1.class);
                intent.putExtra("buy" , s1);
                startActivity(intent);
                overridePendingTransition(0,0);
            }
        });
        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, Therapist_Profile.class));
                overridePendingTransition(0,0);
            }
        });
        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, Daily_diary.class));
                overridePendingTransition(0,0);
            }
        }); cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, Pukaar_Forum.class));//
                overridePendingTransition(0,0);
            }
        });
        set_your_mode_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, Set_Mode.class));
                overridePendingTransition(0,0);
            }
        });
        hamburger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
                overridePendingTransition(0,0);


            }
        });
       navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.left_profile:
                        Intent newIntent = new Intent(getApplicationContext(),Profile.class);
                        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent);
                        overridePendingTransition(0,0);
                        break;

                    case R.id.left_request:
                        Intent newIntent1 = new Intent(getApplicationContext(),Reuest_1on1_session.class);
                        newIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent1);
                        overridePendingTransition(0,0);
                        break;
                    case R.id.left_notification:
                        Intent newIntent2 = new Intent(getApplicationContext(),Notifications.class);
                        newIntent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent2);
                        overridePendingTransition(0,0);
                        break;

                    case R.id.left_about:
                        Intent newIntent4 = new Intent(getApplicationContext(),About.class);
                        newIntent4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent4);
                        overridePendingTransition(0,0);
                        break;
                    case R.id.left_contact:
                        Intent newIntent5 = new Intent(getApplicationContext(),Contact.class);
                        newIntent5.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent5);
                        overridePendingTransition(0,0);
                        break;
                    case R.id.left_change_therapist:
                        Intent newIntent6 = new Intent(getApplicationContext(),Change_therapist.class);
                        newIntent6.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent6);
                        overridePendingTransition(0,0);
                        break;
                  /*  case R.id.chat:
                        Intent newIntent10 = new Intent(getApplicationContext(),Chat.class);
                        newIntent10.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent10);
                        overridePendingTransition(0,0);
                        break;*/
                    case  R.id.left_session_summary:
                        Intent newIntent7 = new Intent(getApplicationContext(),Session_summary.class);
                        newIntent7.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(newIntent7);
                        overridePendingTransition(0,0);
                        break;
                    case  R.id.chat:
                        Intent chat = new Intent(getApplicationContext(),Chat.class);
                        chat.putExtra("id","1");
                        startActivity(chat);
                        overridePendingTransition(0,0);
                        break;
                    case R.id.Logout:
                        CommonFunction commonFunction = null;
                        commonFunction.setToken("" , Dashboard.this);
                        finish();
                        Intent newIntent8 = new Intent(getApplicationContext(),sign_up.class);
                        newIntent8.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(newIntent8);
                }
                return true;
            }
        });

        bottomNavigationView.getMenu().findItem(R.id.nav_home).setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        startActivity(new Intent(getApplication(), Dashboard.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_room:
                        startActivity(new Intent(getApplication(), Session_room.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_profile:
                        startActivity(new Intent(getApplication(), Profile.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.bottom_settting:
                        startActivity(new Intent(getApplication(), Setting.class));
                        overridePendingTransition(0,0);
                        break;


                }
                return false;
            }
        });



}


}