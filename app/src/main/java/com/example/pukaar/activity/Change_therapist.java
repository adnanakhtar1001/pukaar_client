package com.example.pukaar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.pukaar.R;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Change_therapist extends AppCompatActivity {
    @BindView(R.id.change_therapist_button)
    Button change_therapist_button;
    @BindView(R.id.back_arrow_01)
    ImageView back_arrow_01;
    @BindView(R.id.bottom_navigation7)
    BottomNavigationView bottomNavigationView7;
    @BindView(R.id.cht_setting)
    ImageView cht_setting;
    @BindView(R.id.radioButton)
    CheckBox radio;
    @BindView(R.id.radioButton2)
    CheckBox radio2;
    @BindView(R.id.radioButton3)
    CheckBox radio3;
    private APIInterface apiInterface;
    ArrayList<String> reasonList = new ArrayList<>();
    Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_therapist);
        ButterKnife.bind(this);
        dialog =new Dialog(Change_therapist.this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        change_therapist_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.setContentView(R.layout.dialuge_box);
                dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialgue_box);
                dialog.show();
                overridePendingTransition(0,0);

                Button No_button=dialog.findViewById(R.id.no_button);
                No_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        overridePendingTransition(0,0);
                    }
                });
                Button yes_button=dialog.findViewById(R.id.yes_button);
                yes_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                                changeTherapist();



                    }
                });
            }
        });
        back_arrow_01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Change_therapist.this ,Dashboard.class));
            }
        });
        cht_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Change_therapist.this ,Setting.class));
            }
        });

        bottomNavigationView7.getMenu().findItem(R.id.nav_room).setChecked(true);


        bottomNavigationView7.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        startActivity(new Intent(getApplication(), Dashboard.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_room:
                        startActivity(new Intent(getApplication(), Session_room.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_profile:
                        startActivity(new Intent(getApplication(), Profile.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.bottom_settting:
                        startActivity(new Intent(getApplication(), Setting.class));
                        overridePendingTransition(0,0);
                        break;


                }
                return false;
            }
        });
    }

    public void onCheckboxClicked() {

    }

    private void changeTherapist(){
        if (radio.isChecked()){
            reasonList.add(radio.getText().toString());
        }else if (radio2.isChecked()){
            reasonList.add(radio2.getText().toString());
        }else if (radio3.isChecked()){
            reasonList.add(radio3.getText().toString());
        }

        Call<Object> call = apiInterface.changeTherapist(CommonFunction.getToken(getApplicationContext()), reasonList);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Intent newIntent = new Intent(Change_therapist.this,Change_therapist1.class);
                newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(newIntent);
                dialog.dismiss();
                overridePendingTransition(0,0);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "something went wrong", Toast.LENGTH_SHORT).show();

            }
        });

    }
}