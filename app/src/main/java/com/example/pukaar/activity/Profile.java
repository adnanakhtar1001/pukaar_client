package com.example.pukaar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pukaar.R;
import com.example.pukaar.common.CommonFunction;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Profile extends AppCompatActivity {

    @BindView(R.id.back_arrow_9)
    ImageView back_arrow_9;
      @BindView(R.id.up_setting)
    ImageView up_setting;

    @BindView(R.id.bottom_navigation3)
    BottomNavigationView bottomNavigationView3;
    @BindView(R.id.edit_profile)
    TextView edit_profile;

    @BindView(R.id.pateint_name)
    TextView pateint_name;
    @BindView(R.id.patenient_email)
    TextView patenient_email;
    private CommonFunction commonFunction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        String first_name =commonFunction.getname(this);
        String user_email = commonFunction.getEmail(this);
       pateint_name.setText(first_name);
        patenient_email.setText(user_email);


        back_arrow_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Profile.this ,Dashboard.class));
                overridePendingTransition(0,0);
            }
        });
        up_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   Intent intent = new Intent(Profile.this ,Setting.class);

                startActivity(intent);

                overridePendingTransition(0,0);
            }
        });

        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Profile.this ,Edit_user_profile.class));
                overridePendingTransition(0,0);
            }
        });

        bottomNavigationView3.getMenu().findItem(R.id.nav_profile).setChecked(true);


        bottomNavigationView3.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        startActivity(new Intent(getApplication(), Dashboard.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_room:
                        startActivity(new Intent(getApplication(), Session_room.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_profile:
                        startActivity(new Intent(getApplication(), Profile.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.bottom_settting:
                        startActivity(new Intent(getApplication(), Setting.class));
                        overridePendingTransition(0,0);
                        break;


                }
                return false;
            }
        });


    }
}