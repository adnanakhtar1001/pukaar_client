package com.example.pukaar.apiinterface;

import com.example.pukaar.ModelClasses.GetMessage;
import com.example.pukaar.common.CommonFunction;
import com.example.pukaar.response.BankDetailResponse;
import com.example.pukaar.response.DiaryResponse;
import com.example.pukaar.response.ForumResponse;
import com.example.pukaar.response.LoginResponse;
/*import com.example.pukaar.response.MessageResponse;*/
import com.example.pukaar.response.MessageResponse;
import com.example.pukaar.response.NotificationResponse;
import com.example.pukaar.response.SignUpResponse;
import com.example.pukaar.response.SpecialOfferResponse;
import com.example.pukaar.response.Therapist_responce;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIInterface {
 @FormUrlEncoded
 @POST("login")
 Call<LoginResponse> loginCall(@Field("email") String name, @Field("password") String job);

 @FormUrlEncoded
 @POST("register")
 Call<SignUpResponse> signUpCall(@Field("first_name") String fName, @Field("last_name") String lName, @Field("email") String email, @Field("mobile_number") String name, @Field("password") String password, @Field("c_password") String Cpassword, @Field("role") String role);

 @FormUrlEncoded
 @POST("client")
 Call<String> clientCreateProfile(@Header("Authorization") String authorization, @Field("orientation") String orientation, @Field("religion") String religion, @Field("religion_identifier") String religionIdentifier, @Field("medicines") String medicines, @Field("sleeping_habit") String sleepingHabit, @Field("problem") String problem);
 @FormUrlEncoded
 @POST("diary/filters")
 Call<DiaryResponse> getDiaryData(@Header("Authorization") String authorization , @Field("start_date") String start_date , @Field("end_date") String end_date , @Field("number_of_records") String number_of_records);

 @FormUrlEncoded
 @POST("diary")
 Call<String> saveDiaryData(@Header("Authorization") String authorization,@Field("mood") String mood,@Field("anxiety") String anxiety,@Field("energy") String energy,@Field("self_confidence") String self_confidence,@Field("feeling") String feeling);

 @Multipart
 @POST("session")
 Call<String> saveSessionData(@Header("Authorization") String authorization,@Part("picture\"; filename=\"pp.png") RequestBody file,@Part("number_of_sessions") RequestBody numberSession,@Part("cost") RequestBody cost);

 @Multipart
  @POST("session")
  Call<String> saveDonationData(@Header("Authorization") String authorization,@Part("picture\"; filename=\"pp.png") RequestBody file,@Part("number_of_sessions") RequestBody numberSession,@Part("cost") RequestBody cost,@Part("donation") RequestBody donation);

 @GET("package")
 Call<ArrayList<SpecialOfferResponse>> getSpecialOffer(@Header("Authorization") String authorization);

 @GET("bank")
 Call<ArrayList<BankDetailResponse>> getBankDetail(@Header("Authorization") String authorization);

 @Multipart
 @POST("post")
 Call<String> createPost(@Header("Authorization") String authorization,@Part("content") RequestBody content,@Part("picture\"; filename=\"pp.png") RequestBody file);

 @GET("forum")
 Call<ForumResponse> getForumList(@Header("Authorization") String authorization);

 @FormUrlEncoded
 @POST("comment")
 Call<String> createComment(@Header("Authorization") String authorization,@Field("comment") String comment, @Field("post_id") int postId);

 @GET("get-notifications/2")
 Call<ArrayList<NotificationResponse>> getNotification(@Header("Authorization") String authorization);

 @FormUrlEncoded
 @POST("send-message")
 Call<MessageResponse> sendMessage(@Header("Authorization") String authorization, @Field("reciever_id") String receiverId, @Field("message")  String message);

 @FormUrlEncoded
 @POST("get-message")
 Call<GetMessage> getMessage(@Header("Authorization") String authorization, @Field("reciever_id") int receiverId);

 @GET("client?type=therapist_only")
 Call<Therapist_responce> getTherapist(@Header("Authorization") String authorization);
 @FormUrlEncoded
 @POST("client/profile/update")
 Call<String> updateProfile(@Header("Authorization") String authorization , @Field("first_name") String first_name , @Field("last_name") String last_name , @Field("email") String email);

 @FormUrlEncoded
 @POST("onesessionrequest")
 Call<Object> OneonOneSession(@Header("Authorization") String authorization , @Field("session_require_time") String session_require_time );

 @FormUrlEncoded
 @POST("changeTherapist")
 Call<Object> changeTherapist(@Header("Authorization") String authorization , @Field("change_therapist_reason") ArrayList<String> reasonList );

}
