package com.example.pukaar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pukaar.R;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;
import com.example.pukaar.response.FirstDatum;
import com.example.pukaar.response.MessageResponse;
import com.example.pukaar.response.NotificationResponse;

import java.util.ArrayList;

public class inbox_Adapter extends RecyclerView.Adapter<inbox_viewholder> {
    private ArrayList<FirstDatum> message_responce;
    private APIInterface apiInterface;
    Context context1;
    private LayoutInflater mInflater;
    public inbox_Adapter(Context context1, ArrayList<FirstDatum> message_responce){
        this.mInflater = LayoutInflater.from(context1);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        this.message_responce = message_responce;
        this.context1 = context1;
    }

    @NonNull
    @Override
    public inbox_viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.inbox_layout, parent, false);
        return new inbox_viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull inbox_viewholder holder, int position) {
        holder.textview1.setText(message_responce.get(position).senderName);
        holder.data1.setText(message_responce.get(position).content);
     holder.itemView.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {

         }
     });
    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
class inbox_viewholder extends RecyclerView.ViewHolder {
    TextView textview1 , data1;


    public inbox_viewholder(@NonNull View itemView) {
        super(itemView);
        textview1 = itemView.findViewById(R.id.ibox_chat_name);
        data1    = itemView.findViewById(R.id.ibox_chat_time);


    }
}
