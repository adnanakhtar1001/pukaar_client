package com.example.pukaar.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pukaar.R;
import com.example.pukaar.activity.Buy_session1;

import com.example.pukaar.activity.Buy_session2;
import com.example.pukaar.response.BankDetailResponse;
import com.example.pukaar.response.SpecialOfferResponse;

import java.util.ArrayList;
import java.util.List;

public class Speacial_Offers_Adapters extends RecyclerView.Adapter<Speacial_Offers_Adapters.viewholder> {

    private String buy="";
    private List<SpecialOfferResponse> specialOfferResponses;


    private  ItemClickListener itemClickListener1;
    private LayoutInflater mInflater;
    Context context1;
    private BankDetailResponse bankDetailData;


    public Speacial_Offers_Adapters(Context context, ArrayList<SpecialOfferResponse> list, String buy) {
        this.mInflater = LayoutInflater.from(context);
        this.context1 =context;
        this.specialOfferResponses = list;
        this.buy = buy;

    }

    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.speacial_offer_layout, parent, false);
        return new viewholder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull viewholder holder,  int position) {

        holder.total_sessions.setText(specialOfferResponses.get(position).getSession().toString());
        holder.price.setText(specialOfferResponses.get(position).getPrice());

        if (position % 3 == 0){
            holder.cardView.setBackgroundColor(R.color.grey);
        }
        else if (position% 3 == 1){
            holder.cardView.setBackgroundColor(R.color.red);
        }
       else if (position% 3 == 2){
            holder.cardView.setBackgroundColor(R.color.black);
        }

       holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {


               Intent intent = new Intent(context1 , Buy_session2.class);
               intent.putExtra("quantity", specialOfferResponses.get(position).getSession().toString());
               intent.putExtra("total Price" ,specialOfferResponses.get(position).getPrice());
               intent.putExtra("buy" ,buy);

              if (bankDetailData != null) {

                  intent.putExtra("bank name", bankDetailData.getBankName().toString());
                  intent.putExtra("branch name", bankDetailData.getBranchName().toString());
                  intent.putExtra("account number", bankDetailData.getAccountNumber().toString());
                  intent.putExtra("account title", bankDetailData.getAccountTitle().toString());
                  intent.putExtra("iban", bankDetailData.getIban().toString());
              }
               intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               context1.startActivity(intent);
           }
       });
    }

    @Override
    public int getItemCount() {
        return specialOfferResponses.size();
    }

    public void setBankDetailData(BankDetailResponse bankDetailData) {
        this.bankDetailData = bankDetailData;
    }



    class viewholder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView total_sessions , price , bankname;
        ConstraintLayout cardView;

        public viewholder(@NonNull View itemView) {
            super(itemView);

            total_sessions = itemView.findViewById(R.id.offer1_sessions);
            price    = itemView.findViewById(R.id.offer1_price);
            cardView =itemView.findViewById(R.id.parent);
            bankname = itemView.findViewById(R.id.bank_name);
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener1 != null){
                itemClickListener1.onItemClick(itemView , getAdapterPosition());
            }
        }
    }
    void setClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener1 = (ItemClickListener) itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


}


